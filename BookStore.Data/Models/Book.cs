﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Data.Models
{
    public class Book
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public DateTime Date { get; set; }
        public string Writer { get; set; }
    }
}
