﻿using BookStore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.Data
{
    public class Seed
    {
        public static void SeedData(AppDbContext context)
        {
            if (!context.Books.Any())
            {
                var InitialBooks = new List<Book>
                {
                    new Book
                    {
                        Title = "The Great Gatsby",
                        Date = DateTime.Now.AddMonths(-2),
                        Description = "American Dream",
                        Category = "drinks",
                        Writer = "F. Scott Fitzgerald",
                       
                    },
                    new Book
                    {
                        Title = "Harry Potter",
                        Date = DateTime.Now.AddMonths(-1),
                        Description = "Kids",
                        Category = "culture",
                        Writer = "J.K. Rowling"
                    },
                    new Book
                    {
                        Title = "1984",
                        Date = DateTime.Now.AddMonths(1),
                        Description = "",
                        Category = "Science Fiction",
                        Writer = "George Orwell",
                    }
                };

                context.Books.AddRange(InitialBooks);
                context.SaveChanges();
            }
        }
    }
}
