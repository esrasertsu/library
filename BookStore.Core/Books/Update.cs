﻿using BookStore.Data;
using BookStore.Data.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookStore.Core.Books
{
    public class Update
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string Category { get; set; }
            public DateTime? Date { get; set; }
            public string Writer { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly AppDbContext _context;
            public Handler(AppDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var book = await _context.Books.FindAsync(request.Id);

                if (book == null)
                    throw new Exception("Couldnt find book");



                book.Title = request.Title ?? book.Title;
                book.Description = request.Description ?? book.Description;
                book.Category = request.Category ?? book.Category;
                book.Date = request.Date ?? book.Date;
                book.Writer = request.Writer ?? book.Writer;

                // _context.Activities.Update(activity);
                var success = await _context.SaveChangesAsync() > 0;

                if (success) return Unit.Value;
                throw new Exception("Problem update book");
            }
        }
    }
}
