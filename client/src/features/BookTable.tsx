import * as React from "react";
import { IBook } from '../models/book'

interface IProps {
    books: IBook[],
    deleteBook: (e: React.SyntheticEvent<HTMLButtonElement>, id: string) => void;
}

const BookTable: React.FC<IProps> = ({ books, deleteBook})  => (
    <table>
        <thead>
            <tr>
                <th>Title</th>
                <th>Writer</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            {books.length > 0 ? (
               books.map(book => (
                   <tr key={book.id}>
                       <td>{book.title}</td>
                       <td>{book.writer}</td>
                        <td>
                            <button className="button muted-button">Edit</button>
                           <button className="button muted-button" name={book.id} onClick={(e) => deleteBook(e, book.id)}>Delete</button>
                        </td>
                    </tr>
                ))
            ) : (
                    <tr>
                        <td colSpan={3}>No books</td>
                    </tr>
                )}
        </tbody>
    </table>
)

export default BookTable