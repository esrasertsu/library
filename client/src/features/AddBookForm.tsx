import React, { useState, FormEvent } from 'react';
import { IBook } from '../models/book';
import { v4 as uuid } from 'uuid';

interface IProps {
    addBook: (book: IBook) => void
}

const AddBookForm: React.FC<IProps> = ({ addBook }) => {
    const initialFormState = {
                id: '',
                title: '',
                category: '',
                description: '',
                date: '',
                writer: ''
            };
    
    const [book, setBook] = useState(initialFormState)

    const handleInputChange = (event: { currentTarget: { name: any; value: any; }; }) => {
        const { name, value } = event.currentTarget;
        setBook({ ...book, [name]: value });
    }

    const handleSubmit = () => {
        if (book.id.length === 0) {
            let newBook = {
                ...book,
                id: uuid()
            }
            addBook(newBook);
        } 
    }


    return (
        <form onSubmit={handleSubmit}>
            <label>Title</label>
            <input type="text" name="title" value={book.title} onChange={handleInputChange} />
            <label>Writer</label>
            <input type="text" name="writer" value={book.writer} onChange={handleInputChange} />
            <label>Description</label>
            <input type="text" name="description" value={book.description} onChange={handleInputChange} />
            <label>Category</label>
            <input type="text" name="category" value={book.category} onChange={handleInputChange} />
            <label>Date</label>
            <input type="text" name="date" value={book.date} onChange={handleInputChange} />
            <button>Add new book</button>
        </form>
    )
}

export default AddBookForm