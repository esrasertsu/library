import axios, { AxiosResponse } from 'axios';
import { IBook } from '../models/book';
axios.defaults.baseURL = 'http://localhost:5000/api';

const responseBody = (response: AxiosResponse) => response.data;

const sleep = (ms: number) => (response: AxiosResponse) =>
    new Promise<AxiosResponse>(resolve => setTimeout(() => resolve(response), ms));

const requests = {
    get: (url: string) => axios.get(url).then(sleep(1000)).then(responseBody),
    post: (url: string, body: {}) => axios.post(url, body).then(sleep(1000)).then(responseBody),
    put: (url: string, body: {}) => axios.put(url, body).then(sleep(1000)).then(responseBody),
    del: (url: string) => axios.delete(url).then(sleep(1000)).then(responseBody)
};

const Books = {
    list: (): Promise<IBook[]> => requests.get('/books'),
    details: (id: string) => requests.get(`/books/${id}`),
    create: (book: IBook) => requests.post('/books', book),
    update: (book: IBook) => requests.put(`/books/${book.id}`, book),
    delete: (id: string) => requests.del(`/books/${id}`)
}

export default {
    Books
}