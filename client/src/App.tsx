import React, { useState, useEffect, SyntheticEvent } from 'react'
import BookTable from './features/BookTable'
import { IBook } from './models/book';
import axios from 'axios';
import AddBookForm from './features/AddBookForm';
import agent from './api/agent';

const App = () => {
    const [books, setBooks] = useState<IBook[]>([]);
    const [target, setTarget] = useState('');

    useEffect(() => {
        agent.Books.list()
            .then(response => {
                let books: IBook[] = [];
                response.forEach((book) => {
                    books.push(book);
                })
                setBooks(books);
            })
    }, []);
   

    const handleCreateActivity = (book: IBook) => {
        agent.Books.create(book).then(() => {
            setBooks([...books, book]);
        });
    }

    const handleEditActivity = (book: IBook) => {
        agent.Books.update(book).then(() => {
            setBooks([...books.filter(a => a.id !== book.id), book])
        });
    }

    const handleDeleteActivity = (event: SyntheticEvent<HTMLButtonElement>, id: string) => {
        setTarget(event.currentTarget.name);
        agent.Books.delete(id).then(() => {
            setBooks([...books.filter(a => a.id !== id)])
        });
    }
   
    return (
        <div className="container">
            <div className="flex-row">
                <div className="flex-large">
                    <h2>Add book</h2>
                    <AddBookForm 
                        addBook={handleCreateActivity} />
                </div>
                <div className="flex-large">
                    <h2>View books</h2>
                    <BookTable deleteBook={handleDeleteActivity} books={books} />
                </div>
            </div>
        </div>
    )
}

export default App