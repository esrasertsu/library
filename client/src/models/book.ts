export interface IBook {
    id: string;
    title: string;
    description: string;
    category: string;
    date: string;
    writer: string;
}